import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import math

f=open("image_and_colorname.txt","a")
percen=[]
clr=[]
def find_histogram(clt):

   
    numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
    (hist, _) = np.histogram(clt.labels_, bins=numLabels)

    hist = hist.astype("float")
    hist /= hist.sum()
    return hist
    
def distance(m,n):
    d  = math.sqrt(pow((m[0] - int(n[0])),2) + pow((m[1] - int(n[1])),2) + pow((m[2] - int(n[2])),2))
    return(int(d))
    
seven_colors = [[40,40,40], [43, 98, 140],[133, 194, 45], [190, 40, 32],[200,200,200],[105,105,105],[190,190,49],[221,136,40]]
#############black0,     blue 1,    	green 2		red3	white4		           grey5		yellow6
col_names = ["black", "blue","green","red","white","grey","yellow","orange"]

    
def ret_index(l):
      mindist = distance(seven_colors[0],l)
      c=0
      for i in range(1,8):
           d = distance(seven_colors[i],l)
           if(d<mindist):
               mindist = d
               c = i
      return(c)


def plot_colors2(hist, centroids):
    bar = np.zeros((50, 300, 3), dtype="uint8")
    startX = 0
    for (percent, color) in zip(hist, centroids):
        clr.append(list((color.astype(int))))
        percen.append(percent)
   





for i in range(100,321):
   print(i)
   img1 = 'crop/'+str(i)+'.png'

   img = cv2.imread(img1)
   img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
   img = img.reshape((img.shape[0] * img.shape[1],3)) #represent as row*column,channel number
   clt = KMeans(n_clusters=3) #cluster number
   clt.fit(img)
   hist = find_histogram(clt)
   plot_colors2(hist, clt.cluster_centers_)
   k=percen.index(min(percen))
   percen.remove(percen[k])
   clr.remove(clr[k])
   j=percen.index(min(percen))
   val=clr[j]
   clridx=ret_index(val)
   print(col_names[clridx])
   f.write('Image'+str(i)+' '+col_names[clridx]+'\n')
   
   clr=[]
   percen=[]
   

