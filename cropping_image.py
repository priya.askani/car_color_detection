import cv2
import numpy as np

i=1
l_img = cv2.imread("black.png")
x_offset=y_offset=590
for i in range(1,321):
  image = cv2.imread('cars/'+str(i)+'.png')
  copy = image.copy()
  gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)			#converted to gray coz many function use gray
  edged = cv2.Canny(gray, 10, 250)					#detects egdes and removes the noises around the edges depending on threshold  min  max values
  kernel = np.ones((5, 5), np.uint8)		#to fill in a black background
  dilation = cv2.dilate(edged, kernel, iterations=1)
  closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel)
  (image, cnts, hiers) = cv2.findContours(closing, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
  cont = cv2.drawContours(copy, cnts, -1, (0, 0, 0), 1, cv2.LINE_AA)
  mask = np.zeros(cont.shape[:2], dtype="uint8") * 255
 
# Draw the contours on the mask
  cv2.drawContours(mask, cnts, -1, (255, 255, 255), -1)
 
# remove the contours from the image and show the resulting images
  img = cv2.bitwise_and(cont, cont, mask=mask)

  
  for c in cnts:
    x, y, w, h = cv2.boundingRect(c)
    #print("helo")
    if w > 30 and h > 30:
        new_img = img[y:y + h, x:x + w]
        s_img = new_img
        s_img = cv2.resize(s_img, (600, 300)) 
        #print('croppedcars/'+str(i)+'.png')
        l_img[y_offset:y_offset+s_img.shape[0],x_offset:x_offset+s_img.shape[1]] = s_img
        cv2.imwrite('crop/'+str(i)+'.png', l_img)


#4 morpological transformations: 1.erosion: erodes foregroung-boundary,shrinks the object 2: dilation: pixel element is atleast one, increases size of object
#4.Closing:dilation followed by erosion removes black dots on forground image
        
